import json
import logging
import subprocess


def main():
    logging.basicConfig(encoding="utf-8", level=logging.INFO)

    # Get nodes information as a json object
    j = json.loads(subprocess.run(["kubectl get nodes -o json"], shell=True, capture_output=True, check=True).stdout)

    # Loop through all nodes and search for GPU taints
    for item in j["items"]:
        try:
            for taint in item["spec"]["taints"]:
                if taint["key"] == "nvidia.com/gpu" and taint["value"] == "true":
                    logging.info(f"Node {item['metadata']['name']} has a GPU")

                    # Force deletion of pods in case node is unclean
                    try:
                        subprocess.run(
                            "kubectl delete pod nvidia-test --grace-period=0 --force",
                            shell=True,
                            check=True,
                            capture_output=True,
                        )
                    except subprocess.CalledProcessError:
                        logging.debug("No nvidia-test pod to delete")

                    try:
                        subprocess.run(
                            "kubectl delete pod cuda-vector-add --grace-period=0 --force",
                            shell=True,
                            check=True,
                            capture_output=True,
                        )
                    except subprocess.CalledProcessError:
                        logging.debug("No cuda-vector-add pod to delete")

                    # Test nvidia-smi working properly (check if it is a GPU vissible to the pod and if it's recognized by the NVIDIA driver)
                    logging.info(f"Testing nvidia-smi on node {item['metadata']['name']}")
                    proc = subprocess.Popen(
                        """kubectl run nvidia-test --rm -i --image=nvidia/cuda:11.0-base --restart=Never --overrides='{"apiVersion": "v1", "spec": {"tolerations": [{"effect": "NoExecute", "key": "nvidia.com/gpu", "value": "true"}], "runtimeClassName": "nvidia", "nodeSelector": {"kubernetes.io/hostname": """
                        + f'"{item["metadata"]["name"]}"'
                        + """ } }}' -- nvidia-smi""",
                        shell=True,
                        stdout=subprocess.PIPE,
                        stderr=subprocess.PIPE,
                    )
                    stdout, stderr = proc.communicate()
                    rc = proc.returncode
                    if rc != 0:
                        logging.error(f"{item['metadata']['name']} nvidia-smi failed with exit code {rc}")
                        logging.error(f"{stderr.decode('utf-8')}")
                    else:
                        logging.info(f"{item['metadata']['name']} nvidia-smi passed")
                        logging.debug(f"{stdout.decode('utf-8')}")

                    # Test CUDA is working properly
                    logging.info(f"Testing CUDA on node {item['metadata']['name']}")
                    proc = subprocess.Popen(
                        """kubectl run cuda-vector-add --request-timeout='120s' --pod-running-timeout='120s' --image=k8s.gcr.io/cuda-vector-add:v0.1 --restart=Never --overrides='{"apiVersion": "v1", "spec": {"tolerations": [{"effect": "NoExecute", "key": "nvidia.com/gpu", "value": "true"}], "runtimeClassName": "nvidia", "nodeSelector": {"kubernetes.io/hostname": """
                        + f'"{item["metadata"]["name"]}"'
                        + """ } }}'; kubectl wait --for=condition=ready pod/cuda-vector-add;  sleep 5; kubectl logs cuda-vector-add""",
                        shell=True,
                        stdout=subprocess.PIPE,
                        stderr=subprocess.PIPE,
                    )
                    stdout, stderr = proc.communicate()
                    rc = proc.returncode
                    if rc != 0:
                        logging.error(f"{item['metadata']['name']} cuda-vector-add failed with exit code {rc}")
                        logging.error(f"{stderr.decode('utf-8')}")
                    else:
                        logging.info(f"{item['metadata']['name']} cuda-vector-add passed")
                        logging.debug(f"{stdout.decode('utf-8')}")

                    # Force deletion of pods in case of leftovers
                    try:
                        subprocess.run(
                            "kubectl delete pod nvidia-test --grace-period=0 --force",
                            shell=True,
                            check=True,
                            capture_output=True,
                        )
                    except subprocess.CalledProcessError:
                        logging.debug("No nvidia-test pod to delete")

                    try:
                        subprocess.run(
                            "kubectl delete pod cuda-vector-add --grace-period=0 --force",
                            shell=True,
                            check=True,
                            capture_output=True,
                        )
                    except subprocess.CalledProcessError:
                        logging.debug("No cuda-vector-add pod to delete")
                else:
                    logging.debug(f"Node {item['metadata']['name']} has no GPU")
        except KeyError:
            logging.debug(f"{item['metadata']['name']} has no taints")


if __name__ == "__main__":
    main()
