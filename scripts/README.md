# Scripts
This folder purpose is to host single use scripts that help testing and doing some action on a given k8s cluster.

## gputest
Check all nodes for a GPU taint of the form `nvidia/gpu=true` and runs a container in the node testing for correct `nvidia-smi` command and a CUDA job.

### Usage
Define the `KUBECONFIG` env variable pointint at the configuration for the Kubernetes cluster you want to test.

Call:
```
python gputest.py
```

This script doesn't need any previously instaled Python libraries.

### Development
Use the provided poetry project to install the python libraries needed for development.