BASE_DIR=$(shell pwd)


OCI_IMAGES ?= ska-cicd-k8s-tools-build-deploy ska-dev-code-server
IMAGE_FOLDER ?= $(error Please set IMAGE_FOLDER to one of the images)

# include makefile targets for release management
-include .make/release.mk
# include makefile targets for OCI Images management
-include .make/oci.mk
# include k8s support
include .make/k8s.mk
# include Helm Chart support
include .make/helm.mk
# include makefile targets for make submodule
-include .make/make.mk
# include makefile targets for Makefile help
-include .make/help.mk
# include your own private variables for custom deployment configuration
-include PrivateRules.mak

# Building OCI images with their own release version
# oci-pre-build:
#  	$(eval RELEASE_CONTEXT_DIR:="images/$(OCI_IMAGE)")

update-sub:  ## update git submodules
	git submodule init
	git submodule update --recursive --remote
	git submodule update --init --recursive

test-images: update-sub ## Run unit tests using BATS
	cd images/$(IMAGE_FOLDER); \
	mkdir -p $(BASE_DIR)/images/$(IMAGE_FOLDER)/build; \
	$(BASE_DIR)/test_libs/bats-core/bin/bats --report-formatter junit -o $(BASE_DIR)/images/$(IMAGE_FOLDER)/build tests
