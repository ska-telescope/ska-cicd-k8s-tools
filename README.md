# ska-k8s-tools

Kubernetes tools for deployment jobs.
This repository provides a set of docker images that are uploaded to artefact.skao.int, and can be used in multiple subprojects.

## Build Deploy Image

This oci image is using ubuntu 22.04 operating system.
This image includes the packages that are needed to run the common CI/CD jobs in SKAO Pipeline Machinery. It has common tools such as `git, bash, curl, wget, zip, ssh, jq, docker` and the following tools so that the jobs can use them as necessary.

- **kubectl**: to deploy and interact with k8s clusters
- **helm**: to deploy and control helm charts
- **git-chglg**: to automate releases
- **release-cli**: to automate releases
- **poetry**: to manage python dependencies
- **conan**: to manage conan/c++ packages
- **xray**: to upload BDD test results to JIRA/XRAY

In addition the image is used for python build, package and publish jobs in contrast to lint and test jobs. This is because this image has couple of system level packages and **does not provide** a clean python environment.
