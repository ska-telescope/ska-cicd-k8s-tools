.. skeleton documentation master file, created by
   sphinx-quickstart on Thu May 17 15:17:35 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SKA K8s Tools
=================

Kubernetes tools for deployment jobs

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README