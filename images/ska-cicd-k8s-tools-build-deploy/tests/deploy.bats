#!/usr/bin/env bats

# Load a library from the `${BATS_TEST_DIRNAME}/test_helper' directory.
#
# Globals:
#   none
# Arguments:
#   $1 - name of library to load
# Returns:
#   0 - on success
#   1 - otherwise
load_lib() {
  local name="$1"
  load "../../../test_libs/${name}/load"
}

load_lib bats-support
load_lib bats-assert


@test "PLACEHOLDER: Docker Build" {
    run echo "test"
    assert_success
}

@test 'PLACEHOLDER: assert_success() $status only successful' {
  run bash -c "echo test"
  assert_success

  # On failure, $status and $output are displayed.
  # -- command failed --
  # status : 23
  # output : Error!
  # --
}






